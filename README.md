# worker thread implementation using c++23 std::stop_source

This repository provides an educational material exemplifying the usage of c++23 cooperative thread termination features.  Please, read more on [twdev.blog](https://twdev.blog/2023/06/stop_source/).
