#include <chrono>
#include <condition_variable>
#include <deque>
#include <functional>
#include <iostream>
#include <mutex>
#include <stop_token>
#include <string>
#include <thread>

using std::placeholders::_1;

class Logger {
public:
  Logger() : t{std::bind(&Logger::run, this, _1)} {}

  void log(std::string msg) {
    std::unique_lock<std::mutex> l{msgsMutex};
    msgs.push_back(std::move(msg));
    msgsCv.notify_one();
  }

private:
  std::mutex msgsMutex;
  std::deque<std::string> msgs;
  std::condition_variable_any msgsCv;
  std::jthread t;

  void run(std::stop_token token) {
    std::stop_callback callback(
        token, [] { std::cout << "*** logger stop requested" << std::endl; });

    while (!token.stop_requested()) {
      std::string msg;
      {
        std::unique_lock<std::mutex> l{msgsMutex};
        if (!msgsCv.wait(l, token, [this]() { return !msgs.empty(); })) {
          break;
        }

        msg = std::move(msgs.front());
        msgs.pop_front();
      }

      std::cout << "*** log: [" << msg << "]" << std::endl;
    }
  }
};

int main(int argc, const char *argv[]) {
  Logger l;

  l.log("hello world");

  std::this_thread::sleep_for(std::chrono::seconds(1));
  return 0;
}
